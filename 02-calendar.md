---
title: Calendar
permalink: /calendar/
redirect_from: /about/calendar
excerpt: Dates and times for past and future Los Angeles Carnivorous Plant Society meetings.
last_modified_at: 2017-08-27T14:28:13-05:00
---

Meetings are held the third Saturday of every even-numbered month. To keep apprised of the latest Los Angeles Carnivorous Plant Society news and announcements, subscribe to the [LACPS Mailing List](/mailing-list), check the [News](/news) page, or [follow LACPS on twitter](https://twitter.com/lacarnivores).

### 2019 Calendar

* February 16 at the [Alhambra Chamber of Commerce building](/meetings)
* April 27 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* June 15 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 17 at the [Alhambra Chamber of Commerce building](/meetings)
* October 19 at the [Alhambra Chamber of Commerce building](/meetings)
* December 21 at the [Alhambra Chamber of Commerce building](/meetings)

### 2018 Calendar

* February 17 at the [Alhambra Chamber of Commerce building](/meetings)
* April 21 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* June 16 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 18 at the [Alhambra Chamber of Commerce building](/meetings)
* October 20 at the [Alhambra Chamber of Commerce building](/meetings)
* December 15 at the [Alhambra Chamber of Commerce building](/meetings)

### 2017 Calendar

* February 18 at the [Alhambra Chamber of Commerce building](/meetings)
* April 8 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* June 17 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 19 at the [Alhambra Chamber of Commerce building](/meetings)
* October 21 at the [Alhambra Chamber of Commerce building](/meetings)
* December 16 at the [Alhambra Chamber of Commerce building](/meetings)


### 2016 Calendar

* February 20 at the [Alhambra Chamber of Commerce building](/meetings)
* April 16 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* June 18 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 20 at the [Alhambra Chamber of Commerce building](/meetings)
* October 15 at the [Alhambra Chamber of Commerce building](/meetings)
* December 17 at the [Alhambra Chamber of Commerce building](/meetings)


### 2015 Calendar

* February 21 at the [Alhambra Chamber of Commerce building](/meetings)
* April 18 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* July 18 at the [Alhambra Chamber of Commerce building](/meetings)
* August 15 at the [Alhambra Chamber of Commerce building](/meetings)
* October 17 at the [Alhambra Chamber of Commerce building](/meetings)
* December 19 at the [Alhambra Chamber of Commerce building](/meetings)


### 2014 Calendar

* February 15 at the [Alhambra Chamber of Commerce building](/meetings)
* April 19 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* June 21 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 16 at the [Alhambra Chamber of Commerce building](/meetings)
* October 18 at the [La Canada YMCA](/news/2014/10/03/october-2014-meeting-location-change.html) (temporary location for this meeting only)
* December 20 at the [Alhambra Chamber of Commerce building](/meetings)


### 2013 Calendar

* February 16 at the [Alhambra Chamber of Commerce building](/meetings)
* April 20 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* June 15 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 17 at the [Alhambra Chamber of Commerce building](/meetings)
* October 19 at the [Alhambra Chamber of Commerce building](/meetings)
* December 21 at the [Alhambra Chamber of Commerce building](/meetings)


### 2012 Calendar

* February 18 at the [Alhambra Chamber of Commerce building](/meetings)
* April 21 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* June 9 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 18 at the [Alhambra Chamber of Commerce building](/meetings)
* October 20 at the [Alhambra Chamber of Commerce building](/meetings)
* December 15 at the [Alhambra Chamber of Commerce building](/meetings)


### 2011 Calendar

* February 19 at the [Alhambra Chamber of Commerce building](/meetings)
* April 16 at the [Alhambra Chamber of Commerce building](/meetings)
* June 18 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 20 at the [Alhambra Chamber of Commerce building](/meetings)
* October 15 at the [Alhambra Chamber of Commerce building](/meetings)
* December 17 at the [Alhambra Chamber of Commerce building](/meetings)


### 2010 Calendar

* February 20 at the [Alhambra Chamber of Commerce building](/meetings)
* April 17 at the [Alhambra Chamber of Commerce building](/meetings)
* June 19 at the [Alhambra Chamber of Commerce building](/meetings)
* August 21 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* October 16 at the [Alhambra Chamber of Commerce building](/meetings)
* December 18 at the [Alhambra Chamber of Commerce building](/meetings)


### 2009 Calendar

* February 21 at the [Alhambra Chamber of Commerce building](/meetings)
* April 18 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* June 20 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 22 at the [Alhambra Chamber of Commerce building](/meetings)
* October 17 at the [Alhambra Chamber of Commerce building](/meetings)
* December 19 at the [Alhambra Chamber of Commerce building](/meetings)


### 2008 Calendar

* February at the [Alhambra Chamber of Commerce building](/meetings)
* April 19 at the [Alhambra Chamber of Commerce building](/meetings)
* June 21 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* August 16 at the [Alhambra Chamber of Commerce building](/meetings)
* October 18 at the [Alhambra Chamber of Commerce building](/meetings)
* December 20 at the [Alhambra Chamber of Commerce building](/meetings)


### 2007 Calendar

* April 21 at the [Alhambra Chamber of Commerce building](/meetings)
* June 16 at the [CSUF Biology Greenhouse Complex](http://biology.fullerton.edu/facilities/greenhouse/)
* August 18 at the [Huntington Library and Botanical Gardens](http://www.huntington.org)
* October 20 at the [Alhambra Chamber of Commerce building](/meetings)
* December 15 at the [Alhambra Chamber of Commerce building](/meetings)

### Earlier meetings

LACPS meetings began in 1994; the full meeting history is not recorded, although Ivan has written a [brief history of the LACPS](/history).
