---
title: Mailing List
permalink: /mailing-list/
excerpt: How to subscribe to the Los Angeles Carnivorous Plant Society mailing list.
last_modified_at: 2017-08-27T14:28:13-05:00
---

![Photograph of Ivan Snyder's venus flytrap - a carnivorous plant - an an LACPS meeting in Los Angeles, California](/sites/default/files/photos/5_17364713682c96f32f38.jpg){:class="profile"}

Should you wish to keep abreast of occasional Los Angeles Carnivorous Plant Society-themed announcements or dare to participate in electronic LA CP discussions, [click here to subscribe to the LACPS Yahoo Group](https://groups.yahoo.com/neo/groups/LACPS/info) or send an email to [LACPS-subscribe@yahoogroups.com](mailto:LACPS-subscribe@yahoogroups.com).

You might also [follow LACPS on twitter](https://twitter.com/lacarnivores).

### Carnivorous Plant Clubs

Other active carnivorous plant clubs in North America include the following:
* [Bay Area Carnivorous Plant Society](http://www.bacps.org)
* [New England Carnivorous Plant Society](http://www.necps.org)
* [Upper Midwest Carnivorous Plant Society](http://umcps.net)
* [San Diego Carnivorous Plant Society](http://www.sandiegocarnivorousplantsociety.com/)

### Other Carnivorous Plant Resources

For yet more verbiage and photos to feed your carnivorous plant frenzy, any of the following could fit the bill:
* [International Carnivorous Plant Society](http://carnivorousplants.org/)
* [CPUK Forum](http://www.cpukforum.com/) (it ain't just for Brits)
* [CP FAQ](http://www.sarracenia.com/faq.html), which includes a list of [other carnivorous plant societies](http://sarracenia.com/faq/faq6100.html).
