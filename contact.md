---
title: Contact
permalink: /contact/
excerpt: Contact information for the Los Angeles Carnivorous Plant Society.
last_modified_at: 2017-08-27T14:28:13-05:00
---

For more information on the Los Angeles Carnivorous Plant Society, join the [LACPS Yahoo Group](https://groups.yahoo.com/neo/groups/LACPS/info) or contact Thomas Johnson at <span class="spamspan">
<span class="u">tjohnson</span>
[at]
<span class="d">cslanet.calstatela[dot]edu</span>
</span>

### Other Carnivorous Plant Clubs

Should you be looking for a CP group in another geographic area, the [CP FAQ](http://www.sarracenia.com/faq.html) maintains a [list of carnivorous plant societies](http://sarracenia.com/faq/faq6100.html).

### About the Website

This website hosted and maintained by Forbes Conrad. Check out [aldrovanda.com for more of his botanical photography](https://aldrovanda.com/). This site is built on [Jekyll](https://jekyllrb.com/) using [Centrarium](http://bencentra.com/centrarium/) and is hosted on [Github Pages](https://github.com/lacps/lacps.github.io).

Should you find any errors of note on this website, please contact Forbes - <span class="spamspan">
<span class="u">info</span>
[at]
<span class="d">lacps [dot] net</span>
</span> (for meeting-related inquiries, please contact Tom or write the [LACPS Yahoo Group](https://groups.yahoo.com/neo/groups/LACPS/info).

### Copyright

All photography © Forbes Conrad unless otherwise stated.
