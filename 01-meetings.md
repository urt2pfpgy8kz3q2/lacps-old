---
title: Meetings
permalink: /meetings/
redirect_from: /where-to-go
excerpt: Information about Los Angeles Carnivorous Plant Society meetings in Southern California.
last_modified_at: 2017-08-27T14:28:13-05:00
---

![DJ poses with a Venus Flytrap, a type of carnivorous plant, at an LACPS meeting in Alhambra, California](/sites/default/files/photos/12_1735684423f53d73014f.jpg){:class="profile"}

Los Angeles Carnivorous Plant Society meetings are the largest forum in Southern California for discussing and trading carnivorous plants. Whether you are a horticulturist, scientist, educator, student, hobbyist or merely someone intrigued by plants that capture and digest prey, an LACPS meeting may just be the perfect place to further your phyto-carnivorous pursuits.  

### What to Expect

Meetings typically feature plants for display, sale and trade as well as a talk on some aspect of carnivorous plant cultivation or habitat. Should you have questions about growing carnivorous plants in Los Angeles, seeing them in the wild, or, say, the phylogeny of [_Drosera_](http://en.wikipedia.org/wiki/Drosera) in subgenus [_Lasiocephala_](http://en.wikipedia.org/wiki/Taxonomy_of_Drosera#Subgenus_Lasiocephala), you should have little trouble finding someone willing to help.

There are some things you won't find at LACPS gatherings - business meetings, committees, officials or fund raising hassles, for instance - there simply aren't any. If you enjoy snoozing thorough the reading of meeting minutes or if you've always wanted to be elected the Vice President of something (anything), try your local Garden Club or African Violet society.

### Where to Go

Most meetings are held at the Alhambra Chamber of Commerce building. Check the [Meeting Calendar](/calendar) or [News](/news) pages for exceptions. To really dive in with both feet, subscribe to the [LACPS Mailing List](/mailing-list) or [follow LACPS on twitter](https://twitter.com/lacarnivores) for periodic announcements. Meetings typically start at 11am and end around 3pm. The cover charge for Alhambra meetings is $3 to pay for the meeting room.

### Address

[104 S. First St., Alhambra, California. U.S.A.](https://www.openstreetmap.org/?mlat=34.09345&mlon=-118.12691#map=19/34.09345/-118.12691&layers=N)

<iframe width="100%" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-118.12782168388368%2C34.09273085356217%2C-118.12599778175355%2C34.09417019295936&amp;layer=mapnik&amp;marker=34.09345052632012%2C-118.1269097328186" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=34.09345&amp;mlon=-118.12691#map=19/34.09345/-118.12691&amp;layers=N">View Larger Map</a></small>
